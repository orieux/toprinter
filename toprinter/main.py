import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from pathlib import Path
from typing import List

import typer
from dynaconf import Dynaconf
from rich.console import Console

settings = Dynaconf(
    envvar_prefix="TOPRINTER",
    settings_files=[
        "toprinter.toml",
        typer.get_app_dir("toprinter") + "/toprinter.toml",
    ],
)

app = typer.Typer()

console = Console()


@app.command()
def send(
    files: List[Path],
    send_from: str = settings.from_email,
    send_to: str = settings.to,
    server: str = settings.server,
    port: int = settings.port,
    username: str = settings.username,
    password: str = typer.Option(..., prompt="SMTP password", hide_input=True),
) -> None:
    """Compose and send email with `files` as attachments."""

    msg = MIMEMultipart()
    msg["From"] = send_from
    msg["To"] = send_to
    msg["Date"] = formatdate(localtime=True)
    msg["Subject"] = "toprinter payload"

    msg.attach(MIMEText("Not applicable"))

    console.print("Create message")
    with console.status("..."):
        for path in files:
            console.print(f"Load {path}")
            part = MIMEBase("application", "octet-stream")
            with open(path, "rb") as file:
                part.set_payload(file.read())
            encoders.encode_base64(part)
            part.add_header(
                "Content-Disposition",
                'attachment; filename="{}"'.format(Path(path).name),
            )
            msg.attach(part)

    console.print(
        f"Sending to [bold red]{send_to}[/] "
        f"from [blue]{username}@{server}::{port}[/] "
        f"as [bold]{send_from}[/]"
    )
    with console.status("..."):
        with smtplib.SMTP_SSL(server, port) as smtp:
            smtp.login(username, password)
            smtp.sendmail(send_from, send_to, msg.as_string())

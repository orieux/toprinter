# To printer

A small Python script that send files as attachment by email.

Made for printers at CentraleSupelec.

# Install

Install with

```sh
pipx install git+https://gitlab-research.centralesupelec.fr/orieux/toprinter.git
```

Upgrade with

```sh
pipx upgrade toprinter
```

# Usage

```sh
toprinter --help
```

After config (or with arguments)

``` sh
toprinter file1.pdf file2.pdf ...
```

# Configuration

A configuration file `toprinter.toml` can be placed in the
`$HOME/.config/toprinter` directory. A sample file is provided in this
repository. You must know your SMTP parameters.

# Notes

The script needs `dynaconf` and `typer` with their respective dependencies. For
a bare-metal version with Python only as dependency look for `toprinter.old.py`.

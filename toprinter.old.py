#!/usr/bin/env python3

import argparse
import getpass
import pathlib
import smtplib
import ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from typing import List

# %% Parameters

# The CS login
FROM = ""

# The SMTP you want
PORT = 465
SERVER = ""
USERNAME = ""

# %% Codes


def send_mail(
    send_from: str,
    send_to: str,
    files: List[pathlib.Path],
    server: str,
    port: int,
    username: str,
    password: str,
    subject: str = "toprinter script payload",
    message: str = "NA",
    use_tls=True,
) -> None:
    """Compose and send email with info and attachments.

    Args:
        send_from (str): from name
        send_to (str): to name
        subject (str): message title
        message (str): message body
        files (list[Path]): list of file paths to be attached to email
        server (str): mail server host name
        port (int): port number
        username (str): server auth username
        password (str): server auth password
        use_tls (bool) : use tls or not (not for CentraleSupélec)
    """

    msg = MIMEMultipart()
    msg["From"] = send_from
    # msg["To"] = email.utils.COMMASPACE.join(send_to) # for multiple 'to' adress
    msg["To"] = send_to
    msg["Date"] = formatdate(localtime=True)
    msg["Subject"] = subject

    msg.attach(MIMEText(message))

    for path in files:
        part = MIMEBase("application", "octet-stream")
        with open(path, "rb") as file:
            part.set_payload(file.read())
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            'attachment; filename="{}"'.format(pathlib.Path(path).name),
        )
        msg.attach(part)

    with smtplib.SMTP_SSL(server, port) as smtp:
        # with smtplib.SMTP(server, port) as smtp:
        # smtp.starttls(context=ssl.create_default_context())
        smtp.login(username, password)
        smtp.sendmail(send_from, send_to, msg.as_string())


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(
        description=f"Send list of files to printer 'imprimante_cs@centralesupelec.fr'."
    )
    PARSER.add_argument(
        "filepaths",
        metavar="file",
        type=pathlib.Path,
        nargs="+",
        help="file to sent",
    )

    ARGS = PARSER.parse_args()

    send_mail(
        send_from=FROM,
        send_to="imprimante_cs@centralesupelec.fr",
        files=ARGS.filepaths,
        server=SERVER,
        port=PORT,
        username=USERNAME,
        password=getpass.getpass(prompt=f"Password for {USERNAME}@{SERVER}: "),
    )
